# Copyright (c) 2012-2024, OpenGeoSys Community (http://www.opengeosys.org)
#            Distributed under a Modified BSD License.
#            See accompanying file LICENSE.txt or
#            http://www.opengeosys.org/project/license
#

from ogstools.definitions import ROOT_DIR

path_2layers_model = str(ROOT_DIR / "../tests/data/feflowlib/2layers_model.fem")
path_box_Neumann = str(ROOT_DIR / "../tests/data/feflowlib/box_3D_neumann.fem")
path_box_Robin = str(
    ROOT_DIR / "../tests/data/feflowlib/box_3D_cauchy_areal.fem"
)
path_2D_HT_model = str(
    ROOT_DIR / "../tests/data/feflowlib/HT_toymodel_Diri.fem"
)
