# logparser

```{eval-rst}
.. sectionauthor:: Tobias Meisel (Helmholtz Centre for Environmental Research GmbH - UFZ)
```

`logparser` provides an easy way to analyze log messages from ogs runs.

## Features

- logs from serial and parallel execution of ogs are supported
- multiple pre-defined analyses (documented in examples)
- easy to customize (own regular expression, custom analyses)
- works well with pandas data framework

## Getting started

- Examples demonstrating the usage of the logparser can be found at: [](../auto_examples/howto_logparser/index).
- You can access the comprehensive API documentation at: [](../reference/ogstools.logparser).
